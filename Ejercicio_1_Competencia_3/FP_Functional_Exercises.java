import java.util.List;

public class FP_Functional_Exercises{

	public static void main(String[] args) {
		List<Integer> numbers = List.of(12,9,13,4,6,2,4,12,15);
		List<String> courses = List.of("Spring","Spring Boot","API","Microservices",
			"AWS","PCF","Azure Spring","Docker","Kubernetes");

		//Ejercicio 1 Imprimir los numeros impares de la Lista
		System.out.println("Ejercicio 1: Impresión de numeros impares");
		numbers.stream()
		.filter(numeros -> numeros % 2 != 0)
		.forEach(numeros -> System.out.print(numeros+", "));
		//Ejercicio 2: Imprimir todos los cursos individualmente

		System.out.print("\n\nEjercicio 2: Imprimir todos los cursos individualmente\n");
		courses.stream().forEach(le -> System.out.print(le+", "));

		//Ejercicio 3: Imprimir los cursos que contengan la palabra Spring
		System.out.println("\n\nEjercicio 3: Imprimir los cursos que contengan la palabra Spring");
		courses.stream()
		.filter(palabra -> palabra.contains("Spring"))
		.forEach(FP_Functional_Exercises::print);

		//Ejercicio 4: Imprimir cursos cuyo nombre tenga al menos 4 letras.
		System.out.println("\nEjercicio 4: Imprimir cursos cuyo nombre tenga al menos 4 letras.");
		
		courses.stream()
		.filter(FP_Functional_Exercises::superior)
		.forEach(FP_Functional_Exercises::print);
		//Ejercicio 5: Imprime los cubos de números impares
		System.out.println("\nEjercicio 5: Imprime los cubos de números impares");
		numbers.stream()
		.filter(numeros -> numeros % 2 != 0)
		.forEach(numeros -> System.out.print((numeros*numeros*numeros)+", "));

		//Ejercicio 6: Imprime el número de caracteres en el nombre de cada curso.
		System.out.println("\n\nEjercicio 6: Imprime el número de caracteres en el nombre de cada curso.");
		courses.stream().forEach(le -> System.out.print(le+" ("+le.length()+"), "));
		System.out.println("\n");

	}
	private static void print(String palabra){
		System.out.println(palabra);
	}

	private static boolean superior(String cadena){
		return (cadena.length()>=4);
	}
}