class Persona {

    private String nombre;
    private String ap_p;
    private String ap_m;
    private int edad;
    private char sexo;
    

    public Persona() {
        super();
    }

    public Persona(String nombre, String ap_p, String ap_m, int edad, char sexo) {
        super();
        this.nombre = nombre;
        this.ap_p = ap_p;
        this.ap_m = ap_m;
        this.edad = edad;
        this.sexo = sexo; 
        

    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setAp_p(String ap_p) {
        this.ap_p = ap_p;
    }

    public void setAp_m(String ap_m) {
        this.ap_m = ap_m;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    

    public String getNombre() {
        return nombre;
    }

    public String getAp_p() {
        return ap_p;
    }

    public String getAp_m() {
        return ap_m;
    }

    public int getEdad() {
        return edad;
    }

    public char getSexo() {
        return sexo;
    }

   

}
