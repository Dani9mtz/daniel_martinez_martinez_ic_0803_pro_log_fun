public class Lambda{
  public static void main(String[] args) {
    Functiontest ft = () -> System.out.println("Hola mundo soy Atom");
    Lambda objeto = new Lambda();
    objeto.miMetodo(ft);
  }

  public void miMetodo(Functiontest parametro){
    parametro.saludar();
  }

}
