public class lambdatest{
  public static void main(String[] args) {
    //Expresión lambda ==> representa un objeto de una interfaz funcional
    Operaciones op =(num1,num2) -> System.out.println("La suma de los numeros es: \n"+(num1 + num2));

    op.imprimeSuma(5,10);
  }
}
