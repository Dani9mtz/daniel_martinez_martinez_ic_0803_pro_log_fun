public class Principal{
	public static void main(String[] args) {
		try{
	   Calculadoralong h = (x, y) -> x + y;	
	   Calculadoraint b = (x, y) -> x - y;
	   Calculadoraint c = (x, y) -> x * y;
       Calculadoralong d = (x, y) -> x / y;
       Calculadoralong e = (x, y) -> x % y;
		
       System.out.println("==> CalculadoraLong --> Resultado = " + Principal.engine_2(h).calculate(4, 5));
       System.out.println("==> CalculadoraInt --> Resultado = " + Principal.engine_1(b).calculate(23, 12));
       System.out.println("==> CalculadoraInt --> Resultado = " + Principal.engine_1(c).calculate(10, 5));
       System.out.println("==> CalculadoraLong --> Resultado = " + Principal.engine_2(d).calculate(6, 3));
       System.out.println("==> CalculadoraLong --> Resultado = " + Principal.engine_2(e).calculate(23, 11));

		}catch(ArithmeticException error){
			System.out.println("La divisiones entre cero son inválidas");
		}
       
    }
    

	//retorno de un objeto de tipo "calculadoraInt"
	private static  Calculadoraint engine_1(Calculadoraint a){
		return a;
	}
      
	//retorno de un objeto de tipo "calculadoraLong"
	private static Calculadoralong engine_2(Calculadoralong a){
		return a;
	}
}
