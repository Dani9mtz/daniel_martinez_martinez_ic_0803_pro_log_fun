

public interface PruebaExamen{

	public default void mensajeHola(){
		System.out.println("Hola mi nombre es: Daniel Martínez Martinez");
	}

	public default void mensajeHola(String cadena){
		System.out.println("La cadena es: " + cadena);

	}

	public static int operacionPrueba(int a, int b){
		
		int resultado = a + b;
		return resultado;
	}

	
	public void operacionPrueba(int x, int y, int z);
}
