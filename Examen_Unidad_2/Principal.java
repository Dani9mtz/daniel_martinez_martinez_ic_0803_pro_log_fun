public class Principal{//1

public static void main(String[] args) {//2

	PruebaExamen pe;//3
	pe=(a,b,c) -> System.out.println("La suma es: "+(a+b+c));//4
	pe.operacionPrueba(7,2,3);//4

	System.out.println("La suma es: "+PruebaExamen.operacionPrueba(9,8));//5

	pe.mensajeHola();//6
	pe.mensajeHola("Ingeniería en sistemas computacionales.");//7
	
	pe = (a,b,c)-> System.out.println("El resultado es: "+(a+(c*b)));//8
	//pe.operacionPrueba(9,8,3);//8---->Puede ser una alternativa, pero la manera en
	//que se envian los valores se define en el punto nueve

	pe.operacionPrueba(3,PruebaExamen.operacionPrueba(5,4),PruebaExamen.operacionPrueba(1,2));//9

	Principal.miMetodo(pe);//12
	

	Principal.miMetodo((d,e,g)->System.out.println("El resultado es: "+(d+(e/g))));//13

	pe = (a,b,c)-> System.out.println("El resultado es: "+(a+((float)b/c)));//16
	

	Principal.miMetodo(pe,2,7,3);


}
    
	private static  void miMetodo(PruebaExamen parametro){//10
        //11
		int x=PruebaExamen.operacionPrueba(5,5);
		int y=PruebaExamen.operacionPrueba(3,2);
		parametro.operacionPrueba(8,x,y);
		
	}

	private static void miMetodo(PruebaExamen arg,int a,int b,int c){//14
		arg.operacionPrueba(a,PruebaExamen.operacionPrueba(b,b),PruebaExamen.operacionPrueba(c,c));//15
	}


	    

}
